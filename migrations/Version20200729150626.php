<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200729150626 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE task_projection (guid UUID NOT NULL, name VARCHAR(36) DEFAULT NULL, status VARCHAR(50) NOT NULL, deadline DATE DEFAULT NULL, assignee UUID DEFAULT NULL, finished_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(guid))');
        $this->addSql('COMMENT ON COLUMN task_projection.deadline IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN task_projection.finished_at IS \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE task_projection');
    }
}
