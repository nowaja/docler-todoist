<?php declare(strict_types=1);

namespace App\Tests\Unit;

use App\Domain\AggregateRoot\Id\TaskId;
use App\Domain\AggregateRoot\Task;
use App\Domain\Entity\Id\UserId;
use App\Domain\Event\TaskDeadlineHasBeenUpdatedEvent;
use App\Domain\Event\TaskHasBeenAssignedToUserEvent;
use App\Domain\Event\TaskHasBeenCreatedEvent;
use App\Domain\Event\TaskHasBeenDeletedEvent;
use App\Domain\Event\TaskHasBeenFinishedEvent;
use App\Domain\Event\TaskHasBeenRenamedEvent;
use App\Infrastructure\EventStore\EventStream;
use PHPStan\Testing\TestCase;
use Ramsey\Uuid\Uuid;

class TaskTest extends TestCase
{
    public function test_it_can_be_created(): void
    {
        $name = 'Test task 1';
        $taskId = new TaskId(Uuid::uuid4());

        // prepared aggregate root
        $task = Task::create($taskId, $name);

        $this->assertEquals(2, count($task->getRecordedEvents()));
        $this->assertInstanceOf(TaskHasBeenCreatedEvent::class, $task->getRecordedEvents()[0]);
        $this->assertInstanceOf(TaskHasBeenRenamedEvent::class, $task->getRecordedEvents()[1]);

        $this->assertEquals($taskId, $task->getTaskId());
        $this->assertEquals($name, $task->getName());
    }

    public function test_it_can_be_created_with_future_deadline(): void
    {
        $name = 'Test task 1';
        $taskId = new TaskId(Uuid::uuid4());
        $deadline = new \DateTimeImmutable('+5days');

        // prepared aggregate root
        $task = Task::create($taskId, $name, $deadline);

        $this->assertEquals(3, count($task->getRecordedEvents()));
        $this->assertInstanceOf(TaskHasBeenCreatedEvent::class, $task->getRecordedEvents()[0]);
        $this->assertInstanceOf(TaskHasBeenRenamedEvent::class, $task->getRecordedEvents()[1]);
        $this->assertInstanceOf(TaskDeadlineHasBeenUpdatedEvent::class, $task->getRecordedEvents()[2]);

        $this->assertEquals($taskId, $task->getTaskId());
        $this->assertEquals($name, $task->getName());
        $this->assertEquals($deadline, $task->getDeadline());
    }

    public function test_it_can_be_created_with_past_deadline(): void
    {
        $name = 'Test task 1';
        $taskId = new TaskId(Uuid::uuid4());
        $deadline = new \DateTimeImmutable('-2days');

        // prepared aggregate root
        $task = Task::create($taskId, $name, $deadline);

        $this->assertEquals(2, count($task->getRecordedEvents()));
        $this->assertInstanceOf(TaskHasBeenCreatedEvent::class, $task->getRecordedEvents()[0]);
        $this->assertInstanceOf(TaskHasBeenRenamedEvent::class, $task->getRecordedEvents()[1]);

        $this->assertEquals($taskId, $task->getTaskId());
        $this->assertEquals($name, $task->getName());
    }

    public function test_it_can_be_assigned(): void
    {
        $name = 'Test task';
        $taskId = new TaskId(Uuid::uuid4());
        $assigneeId = new UserId(Uuid::uuid4());

        $given = [
            new TaskHasBeenCreatedEvent($taskId),
        ];

        /** @var Task $task */
        $task = Task::reconstituteFromEventStream($taskId, new EventStream($given));

        $task->assign($assigneeId);

        $this->assertEquals(1, count($task->getRecordedEvents()));
        $this->assertInstanceOf(TaskHasBeenAssignedToUserEvent::class, $task->getRecordedEvents()[0]);
        $this->assertEquals($assigneeId,  $task->getAssignee());
    }

    public function test_it_cannot_be_assigned_to_same_user(): void
    {
        $taskId = new TaskId(Uuid::uuid4());
        $assignee = new UserId(Uuid::uuid4());

        $given = [
            new TaskHasBeenCreatedEvent($taskId),
            new TaskHasBeenAssignedToUserEvent($taskId, $assignee),
        ];

        /** @var Task $task */
        $task = Task::reconstituteFromEventStream($taskId, new EventStream($given));

        $task->assign($assignee);
        $this->assertEquals(0, count($task->getRecordedEvents()));
    }

    public function test_it_can_get_updated_deadline(): void
    {
        $name = 'Test task';
        $taskId = new TaskId(Uuid::uuid4());
        $deadline = new \DateTimeImmutable('+5days');

        $given = [
            new TaskHasBeenCreatedEvent($taskId),
        ];

        /** @var Task $task */
        $task = Task::reconstituteFromEventStream($taskId, new EventStream($given));

        $task->setDeadline($deadline);

        $this->assertEquals(1, count($task->getRecordedEvents()));
        $this->assertInstanceOf(TaskDeadlineHasBeenUpdatedEvent::class, $task->getRecordedEvents()[0]);
        $this->assertEquals($deadline,  $task->getDeadline());
    }

    public function test_it_cannot_get_deadline_updated_to_past(): void
    {
        $name = 'Test task';
        $taskId = new TaskId(Uuid::uuid4());
        $originalDeadline = new \DateTimeImmutable('+2days');
        $newDeadline = new \DateTimeImmutable('-2days');

        $given = [
            new TaskHasBeenCreatedEvent($taskId),
            new TaskDeadlineHasBeenUpdatedEvent($taskId, $originalDeadline)
        ];

        /** @var Task $task */
        $task = Task::reconstituteFromEventStream($taskId, new EventStream($given));

        $task->setDeadline($newDeadline);

        $this->assertEquals(0, count($task->getRecordedEvents()));
        $this->assertEquals($originalDeadline,  $task->getDeadline());
    }

    public function test_it_can_be_deleted(): void
    {
        $name = 'Test task';
        $taskId = new TaskId(Uuid::uuid4());

        $given = [
            new TaskHasBeenCreatedEvent($taskId),
        ];

        /** @var Task $task */
        $task = Task::reconstituteFromEventStream($taskId, new EventStream($given));

        $task->delete();

        $this->assertEquals(1, count($task->getRecordedEvents()));
        $this->assertInstanceOf(TaskHasBeenDeletedEvent::class, $task->getRecordedEvents()[0]);
        $this->assertTrue($task->isDeleted());
    }

    public function test_it_cannot_be_deleted_again(): void
    {
        $taskId = new TaskId(Uuid::uuid4());
        $deletedAt = new \DateTimeImmutable('now');

        $given = [
            new TaskHasBeenCreatedEvent($taskId),
            new TaskHasBeenDeletedEvent($taskId, $deletedAt),
        ];

        /** @var Task $task */
        $task = Task::reconstituteFromEventStream($taskId, new EventStream($given));

        $task->delete();

        $this->assertEquals(0, count($task->getRecordedEvents()));
    }

    public function test_it_cannot_be_updated_when_deleted(): void
    {
        $taskId = new TaskId(Uuid::uuid4());
        $userId = new UserId(Uuid::uuid4());
        $deletedAt = new \DateTimeImmutable('now');

        $given = [
            new TaskHasBeenCreatedEvent($taskId),
            new TaskHasBeenDeletedEvent($taskId, $deletedAt),
        ];

        /** @var Task $task */
        $task = Task::reconstituteFromEventStream($taskId, new EventStream($given));

        $task->rename('test');
        $this->assertEquals(0, count($task->getRecordedEvents()));

        $task->setDeadline(new \DateTimeImmutable('+1day'));
        $this->assertEquals(0, count($task->getRecordedEvents()));

        $task->assign($userId);
        $this->assertEquals(0, count($task->getRecordedEvents()));
    }

    public function test_it_can_be_finished(): void
    {
        $taskId = new TaskId(Uuid::uuid4());

        $given = [
            new TaskHasBeenCreatedEvent($taskId),
        ];

        /** @var Task $task */
        $task = Task::reconstituteFromEventStream($taskId, new EventStream($given));

        $task->finish();
        $this->assertEquals(1, count($task->getRecordedEvents()));
        $this->assertInstanceOf(TaskHasBeenFinishedEvent::class, $task->getRecordedEvents()[0]);
        $this->assertTrue($task->isFinished());
    }

    public function test_it_cannot_be_finished_again(): void
    {
        $taskId = new TaskId(Uuid::uuid4());
        $finishedAt = new \DateTimeImmutable('now');

        $given = [
            new TaskHasBeenCreatedEvent($taskId),
            new TaskHasBeenFinishedEvent($taskId, $finishedAt)
        ];

        /** @var Task $task */
        $task = Task::reconstituteFromEventStream($taskId, new EventStream($given));

        $task->finish();
        $this->assertEquals(0, count($task->getRecordedEvents()));
    }
}
