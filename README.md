# TODOIST

## Requirements

Docker

## Running the app

```bash
git clone https://gitlab.com/nowaja/docler-todoist.git
cd docler-todoist
docker-compose up -d
docker exec -ti docler-php php composer.phar install
docker exec -ti docler-php bin/console --no-interaction d:m:m
```

After finishing those commands, API is running on `http://localhost:8888/`

## Features
- create Task
- set Task name, assignee, deadline and status (in progress/finished) 
- delete task (soft delete)
- get Task detail
- get Task history

I used Event Sourcing pattern, especially because of `Task` history presentation. All commands/events are synchronously handled by [Symfony Messenger component](https://symfony.com/doc/current/components/messenger.html).
In production environment can be some projections updated to be async (ie. with RabbitMQ).
You can find some `FIXME`s in source code - those are parts I didn't refactored anymore because of lack of time. 

## REST API Endpoints
- `POST /tasks`
- `PUT /tasks/{id}/name`
- `PUT /tasks/{id}/finish`
- `PUT /tasks/{id}/assignee`
- `PUT /tasks/{id}/deadline`
- `GET /tasks`
- `GET /tasks/{id}`
- `GET /tasks/{id}/history`
- `DELETE /tasks/{id}`

[REST API Docs](https://documenter.getpostman.com/view/8757933/T1Dtfbfn)

## Missing features

- add API auth
- add API versioning
- add better API documentation
- pagination in `GET /tasks`
- filtering in `GET /tasks`
- add 1st & 2nd level cache for doctrine metadata/results/queries
- add more statuses (convert status to Value Object)
- add InMemory TaskRepositoryRepository for testing purposes
- add reopen possibility (change from finished back to wip)
- change event store DB
- make PHP dockerfile better and **smaller**
- move composer from repo to docker image
