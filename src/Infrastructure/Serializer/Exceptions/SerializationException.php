<?php declare(strict_types=1);

namespace App\Infrastructure\Serializer\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class SerializationException extends Exception
{
    public function __construct(string $message = "Serialization failed.", int $code = Response::HTTP_INTERNAL_SERVER_ERROR, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
