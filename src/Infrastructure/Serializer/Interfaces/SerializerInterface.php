<?php declare(strict_types=1);

namespace App\Infrastructure\Serializer\Interfaces;

interface SerializerInterface
{
    public function serialize($object): array;

    public function deserialize(array $serializedObject);
}
