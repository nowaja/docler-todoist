<?php declare(strict_types=1);

namespace App\Infrastructure\Serializer;

use App\Infrastructure\Serializer\Exceptions\SerializationException;
use App\Infrastructure\Serializer\Interfaces\SerializerInterface;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

/**
 * This file is originally part of prooph.org
 */
class ReflectionSerializer implements SerializerInterface
{
    public function serialize($object): array
    {
        return $this->serializeObjectRecursively($object);
    }

    private function serializeValue($value)
    {
        if (is_object($value)) {
            return $this->serializeObjectRecursively($value);
        } elseif (is_array($value)) {
            return $this->serializeArrayRecursively($value);
        }

        return $value;
    }

    private function serializeArrayRecursively(array $array): array
    {
        $data = [];
        foreach ($array as $key => $value) {
            $data[$key] = $this->serializeValue($value);
        }

        return $data;
    }

    /**
     * @param $object
     * @return array
     * @throws ReflectionException
     */
    private function serializeObjectRecursively($object): array
    {
        $reflection = new ReflectionClass($object);
        $properties = $reflection->getProperties();

        $data = [];
        foreach ($properties as $property) {
            $name = $property->getName();

            $property->setAccessible(true);
            $value = $property->getValue($object);
            $property->setAccessible(false);

            $data[$name] = $this->serializeValue($value);
        }

        if (get_class($object) === 'DateTimeImmutable') {
            $data = $object->format('Y-m-d H:i:s');
        }

        return [
            'class' => get_class($object),
            'payload' => $data,
        ];
    }

    public function deserialize(array $serializedObject)
    {
        return $this->deserializeObjectRecursively($serializedObject);
    }

    private function deserializeValue($value)
    {
        if (is_array($value) && isset($value['class']) && isset($value['payload'])) {
            return $this->deserializeObjectRecursively($value);
        } elseif (is_array($value)) {
            return $this->deserializeArrayRecursively($value);
        }

        return $value;
    }

    private function deserializeArrayRecursively(array $array): array
    {
        $data = [];
        foreach ($array as $key => $value) {
            $data[$key] = $this->deserializeValue($value);
        }

        return $data;
    }

    /**
     * @param $serializedObject
     * @return object|void
     * @throws ReflectionException
     * @throws SerializationException
     */
    private function deserializeObjectRecursively($serializedObject)
    {
        if (!array_key_exists('class', $serializedObject)) {
            throw new SerializationException(sprintf('Object could not be deserialized, "class" key not found.'));
        }

        if (!array_key_exists('class', $serializedObject)) {
            throw new SerializationException(sprintf('Object could not be deserialized, "payload" key not found for object "%s".', $serializedObject['class']));
        }

        $reflection = new ReflectionClass($serializedObject['class']);
        $properties = $reflection->getProperties();
        $object = $reflection->newInstanceWithoutConstructor();

        // FIXME - only hotfix, should use better serialization
        if ($serializedObject['class'] === 'DateTimeImmutable') {
            return new \DateTimeImmutable($serializedObject['payload']);
        }

        foreach ($serializedObject['payload'] as $name => $value) {
            $matchedProperty = $this->findProperty($properties, $name);

            if (null === $matchedProperty) {
                throw new SerializationException(sprintf('Property "%s" not found for object "%s"', $name, $serializedObject['class']));
            }

            $value = $this->deserializeValue($value);

            $matchedProperty->setAccessible(true);
            $matchedProperty->setValue($object, $value);
            $matchedProperty->setAccessible(false);
        }

        return $object;
    }

    /**
     * @param ReflectionProperty[] $properties
     * @param string $name
     * @return ReflectionProperty|null
     */
    private function findProperty(array $properties, string $name): ?ReflectionProperty
    {
        foreach ($properties as $property) {
            if ($property->getName() === $name) {
                return $property;
            }
        }

        return null;
    }
}
