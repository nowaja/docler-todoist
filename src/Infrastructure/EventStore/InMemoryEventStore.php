<?php declare(strict_types=1);

namespace App\Infrastructure\EventStore;

use App\Domain\Event\Interfaces\EventInterface;
use App\Infrastructure\EventStore\Interfaces\EventStoreInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class InMemoryEventStore implements EventStoreInterface
{
    private array $events;

    private MessageBusInterface $eventBus;

    public function __construct(MessageBusInterface $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function append(array $events, int $playhead): void
    {
        foreach ($events as $event) {
            if (!$event instanceof EventInterface) {
                continue;
            }

            $this->events[(string)$event->getAggregateId()][] = $event;
        }

        $this->dispatch($events);
    }

    public function load(string $uuid): ?EventStream
    {
        if (isset($this->events[$uuid])) {
            return new EventStream($this->events[$uuid]);
        }

        return null;
    }

    public function dispatch(array $events): void
    {
        foreach ($events as $event) {
            if (!$event instanceof EventInterface) {
                continue;
            }

            $this->eventBus->dispatch($event);
        }
    }
}
