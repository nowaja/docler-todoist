<?php declare(strict_types=1);

namespace App\Infrastructure\EventStore;

use App\Domain\Event\Interfaces\EventInterface;
use App\Infrastructure\EventStore\Interfaces\EventStoreInterface;
use App\Infrastructure\Serializer\Interfaces\SerializerInterface;
use App\ReadModel\Entity\EventStoreRecord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Messenger\MessageBusInterface;

class OrmEventStore extends ServiceEntityRepository implements EventStoreInterface
{
    private MessageBusInterface $eventBus;

    private SerializerInterface $serializer;

    public function __construct(MessageBusInterface $eventBus, SerializerInterface $serializer, ManagerRegistry $registry)
    {
        $this->eventBus = $eventBus;
        $this->serializer = $serializer;

        parent::__construct($registry, EventStoreRecord::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @param array $events
     * @param int $lastPlayhead
     */
    public function append(array $events, int $lastPlayhead): void
    {
        foreach ($events as $event) {
            if (!$event instanceof EventInterface) {
                continue;
            }

            $serializedEvent = \json_encode($this->serializer->serialize($event));
            if (!$serializedEvent) {
                throw new \Exception('JSON serialization failed', 500); // FIXME custom exceptions
            }

            $eventStoreRecord = new EventStoreRecord($event, ++$lastPlayhead, $serializedEvent);
            $this->getEntityManager()->persist($eventStoreRecord);
        }

        $this->getEntityManager()->flush();
        $this->dispatch($events);
    }

    public function load(string $uuid): ?EventStream
    {
        $eventStoreRecords = $this->findBy(
            [
                'guid' => $uuid,
            ],
            [
                'playhead' => 'ASC',
            ]
        );

        $events = [];

        /** @var EventStoreRecord $eventStoreRecord */
        foreach ($eventStoreRecords as $eventStoreRecord) {
            $events[] = $this->serializer->deserialize(json_decode($eventStoreRecord->getSerializedEvent(), true));
        }

        if (count($events) > 0) {
            return new EventStream($events);
        }

        return null;
    }

    public function dispatch(array $events): void
    {
        foreach ($events as $event) {
            if (!$event instanceof EventInterface) {
                continue;
            }

            $this->eventBus->dispatch($event);
        }
    }
}
