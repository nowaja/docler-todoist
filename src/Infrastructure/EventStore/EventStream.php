<?php declare(strict_types=1);

namespace App\Infrastructure\EventStore;

class EventStream
{
    private array $events;

    public function __construct(array $events)
    {
        $this->events = $events;
    }

    public function getEvents(): array
    {
        return $this->events;
    }
}
