<?php declare(strict_types=1);

namespace App\Infrastructure\EventStore\Interfaces;

use App\Infrastructure\EventStore\EventStream;

interface EventStoreInterface
{
    public function load(string $uuid): ?EventStream;

    public function append(array $events, int $lastPlayhead): void;

    public function dispatch(array $events): void;
}
