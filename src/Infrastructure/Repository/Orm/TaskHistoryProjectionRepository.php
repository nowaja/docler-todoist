<?php declare(strict_types=1);

namespace App\Infrastructure\Repository\Orm;

use App\Domain\AggregateRoot\Id\TaskId;
use App\ReadModel\Entity\TaskHistoryProjection;
use App\ReadModel\Repository\Interfaces\TaskHistoryProjectionRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TaskHistoryProjectionRepository extends ServiceEntityRepository implements TaskHistoryProjectionRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskHistoryProjection::class);
    }

    /**
     * @param string $taskGuid
     * @return TaskHistoryProjection[]
     */
    public function findByTaskGuid(string $taskGuid): array
    {
        return $this->findBy([
            'taskId' => $taskGuid,
        ]);
    }

    /**
     * @param TaskHistoryProjection $taskHistory
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(TaskHistoryProjection $taskHistory): void
    {
        $this->getEntityManager()->persist($taskHistory);
        $this->getEntityManager()->flush();
    }
}
