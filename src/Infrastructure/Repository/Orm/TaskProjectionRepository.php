<?php declare(strict_types=1);

namespace App\Infrastructure\Repository\Orm;

use App\Domain\AggregateRoot\Id\TaskId;
use App\Domain\Entity\Id\UserId;
use App\ReadModel\Entity\TaskProjection;
use App\ReadModel\Repository\Interfaces\TaskProjectionRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TaskProjectionRepository extends ServiceEntityRepository implements TaskProjectionRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskProjection::class);
    }

    public function findByTaskId(TaskId $taskId): ?TaskProjection
    {
        /** @var TaskProjection|null*/
        return $this->findOneBy([
            'guid' => (string) $taskId,
        ]);
    }

    public function findByTaskGuid(string $taskGuid): ?TaskProjection
    {
        /** @var TaskProjection|null*/
        return $this->findOneBy([
            'guid' => $taskGuid,
        ]);
    }

    public function getTasksByAssignee(UserId $userId): array
    {
        return $this->findBy([
            'assignee' => (string) $userId,
        ]);
    }

    /**
     * @param TaskProjection $taskHistory
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(TaskProjection $taskHistory): void
    {
        $this->getEntityManager()->persist($taskHistory);
        $this->getEntityManager()->flush();
    }

    /**
     * @param TaskProjection $task
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(TaskProjection $task): void
    {
        $this->getEntityManager()->remove($task);
        $this->getEntityManager()->flush();
    }


}
