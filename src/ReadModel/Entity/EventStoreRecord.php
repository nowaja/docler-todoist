<?php declare(strict_types=1);

namespace App\ReadModel\Entity;

use App\Domain\Event\Interfaces\EventInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\EventStore\OrmEventStore")
 */
final class EventStoreRecord
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=36)
     */
    private string $guid;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private int $playhead;

    /**
     * @ORM\Column(type="text")
     */
    private string $serializedEvent;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $recordedAt;

    public function __construct(EventInterface $event, int $playhead, string $serializedEvent)
    {
        $this->guid = (string) $event->getAggregateId();
        $this->playhead = $playhead;
        $this->serializedEvent = $serializedEvent;
        $this->recordedAt = new \DateTimeImmutable();
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function getPlayhead(): int
    {
        return $this->playhead;
    }

    public function getSerializedEvent(): string
    {
        return $this->serializedEvent;
    }

    public function getRecordedAt(): \DateTimeImmutable
    {
        return $this->recordedAt;
    }
}
