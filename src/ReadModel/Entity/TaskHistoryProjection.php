<?php declare(strict_types=1);

namespace App\ReadModel\Entity;

use App\Domain\AggregateRoot\Id\TaskId;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\Orm\TaskProjectionRepository")
 */
final class TaskHistoryProjection
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private int $id;

    /**
     * @ORM\Column(type="guid", length=36)
     */
    private string $taskId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $eventDescription;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $createdAt;

    public function __construct(TaskId $taskId, string $eventDescription)
    {
        $this->taskId = (string) $taskId;
        $this->eventDescription = $eventDescription;
        $this->createdAt = new \DateTimeImmutable('now');
    }

    public static function create(TaskId $taskId, string $eventDescription): self
    {
        return new self($taskId, $eventDescription);
    }

    public function __toString(): string
    {
        return $this->eventDescription;
    }

    public function toArray(): array
    {
        return [
            'taskId' => $this->taskId,
            'createdAt' => $this->createdAt->format('Y-m-d H:i:s'),
            'eventDescription' => $this->eventDescription,
        ];
    }
}
