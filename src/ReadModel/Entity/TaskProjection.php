<?php declare(strict_types=1);

namespace App\ReadModel\Entity;

use App\Domain\AggregateRoot\Id\TaskId;
use App\Domain\Entity\Id\UserId;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\Orm\TaskProjectionRepository")
 */
final class TaskProjection
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid", length=36)
     */
    private string $guid;

    /**
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $status = 'In Progress';

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $deadline = null;

    /**
     * @ORM\Column(type="guid", length=36, nullable=true)
     */
    private ?string $assignee = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $finishedAt = null;

    public function __construct(TaskId $taskId)
    {
        $this->guid = (string) $taskId;
    }

    public static function create(TaskId $taskId): self
    {
        return new self($taskId);
    }

    public function rename(string $name): void
    {
        $this->name = $name;
    }

    public function setDeadline(?\DateTimeImmutable $deadline): void
    {
        $this->deadline = $deadline;
    }

    public function assign(UserId $assignee): void
    {
        $this->assignee = (string) $assignee;
    }

    public function finish(\DateTimeImmutable $finishedAt): void
    {
        $this->status = 'Finished'; // FIXME
        $this->finishedAt = $finishedAt;
    }

    /**
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getDeadline(): ?\DateTimeImmutable
    {
        return $this->deadline;
    }

    public function getAssignee(): ?string
    {
        return $this->assignee;
    }

    public function getFinishedAt(): ?\DateTimeImmutable
    {
        return $this->finishedAt;
    }

    public function __toString(): string
    {
        return $this->guid;
    }

    public function toArray(): array
    {
        return [
            'taskId' => $this->guid,
            'name' => $this->name,
            'status' => $this->status,
            'assignee' => (string) $this->assignee,
            'deadline' => $this->deadline ? $this->deadline->format('Y-m-d') : null,
            'finishedAt' => $this->finishedAt ? $this->finishedAt->format('Y-m-d H:i:s') : null,
        ];
    }
}
