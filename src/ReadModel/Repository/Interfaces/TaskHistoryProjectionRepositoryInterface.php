<?php declare(strict_types=1);

namespace App\ReadModel\Repository\Interfaces;

use App\Domain\AggregateRoot\Id\TaskId;
use App\ReadModel\Entity\TaskHistoryProjection;

interface TaskHistoryProjectionRepositoryInterface
{
    public function findByTaskGuid(string $taskGuid): array;

    public function save(TaskHistoryProjection $taskHistory): void;
}
