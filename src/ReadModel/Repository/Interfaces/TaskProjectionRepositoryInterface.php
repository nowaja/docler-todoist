<?php declare(strict_types=1);

namespace App\ReadModel\Repository\Interfaces;

use App\Domain\AggregateRoot\Id\TaskId;
use App\Domain\Entity\Id\UserId;
use App\ReadModel\Entity\TaskProjection;
use Doctrine\Persistence\ObjectRepository;

interface TaskProjectionRepositoryInterface extends ObjectRepository
{
    public function findByTaskId(TaskId $taskId): ?TaskProjection;

    public function findByTaskGuid(string $taskGuid): ?TaskProjection;

    public function getTasksByAssignee(UserId $userId): array;

    public function save(TaskProjection $taskHistory): void;

    public function delete(TaskProjection $task): void;
}
