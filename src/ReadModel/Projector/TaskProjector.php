<?php declare(strict_types=1);

namespace App\ReadModel\Projector;

use App\Domain\Event\TaskDeadlineHasBeenUpdatedEvent;
use App\Domain\Event\TaskHasBeenAssignedToUserEvent;
use App\Domain\Event\TaskHasBeenCreatedEvent;
use App\Domain\Event\TaskHasBeenDeletedEvent;
use App\Domain\Event\TaskHasBeenFinishedEvent;
use App\Domain\Event\TaskHasBeenRenamedEvent;
use App\ReadModel\Entity\TaskProjection;
use App\ReadModel\Repository\Interfaces\TaskProjectionRepositoryInterface;

final class TaskProjector extends AbstractProjector
{
    private TaskProjectionRepositoryInterface $taskProjectionRepository;

    public function __construct(TaskProjectionRepositoryInterface $taskProjectionRepository)
    {
        $this->taskProjectionRepository = $taskProjectionRepository;
    }

    // projection apply methods

    public function applyTaskHasBeenCreatedEvent(TaskHasBeenCreatedEvent $event): void
    {
        $task = $this->taskProjectionRepository->findByTaskId($event->getTaskId());

        if ($task) {
           return;
        }

        $task = TaskProjection::create($event->getTaskId());

        $this->taskProjectionRepository->save($task);
    }

    public function applyTaskHasBeenRenamedEvent(TaskHasBeenRenamedEvent $event): void
    {
        $task = $this->taskProjectionRepository->findByTaskId($event->getTaskId());

        if (!$task) {
            return;
        }

        $task->rename($event->getName());

        $this->taskProjectionRepository->save($task);
    }

    public function applyTaskDeadlineHasBeenUpdatedEvent(TaskDeadlineHasBeenUpdatedEvent $event): void
    {
        $task = $this->taskProjectionRepository->findByTaskId($event->getTaskId());

        if (!$task) {
            return;
        }

        $task->setDeadline($event->getDeadline());

        $this->taskProjectionRepository->save($task);
    }

    public function applyTaskHasBeenAssignedToUserEvent(TaskHasBeenAssignedToUserEvent $event): void
    {
        $task = $this->taskProjectionRepository->findByTaskId($event->getTaskId());

        if (!$task) {
            return;
        }

        $task->assign($event->getAssignee());

        $this->taskProjectionRepository->save($task);
    }

    public function applyTaskHasBeenFinishedEvent(TaskHasBeenFinishedEvent $event): void
    {
        $task = $this->taskProjectionRepository->findByTaskId($event->getTaskId());

        if (!$task) {
            return;
        }

        $task->finish($event->getFinishedAt());

        $this->taskProjectionRepository->save($task);
    }

    public function applyTaskHasBeenDeletedEvent(TaskHasBeenDeletedEvent $event): void
    {
        $task = $this->taskProjectionRepository->findByTaskId($event->getTaskId());

        if (!$task) {
            return;
        }

        $this->taskProjectionRepository->delete($task);
    }
}
