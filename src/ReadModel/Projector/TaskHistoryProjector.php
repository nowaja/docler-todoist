<?php declare(strict_types=1);

namespace App\ReadModel\Projector;

use App\Domain\Event\AbstractTaskEvent;
use App\Domain\Event\Interfaces\EventInterface;
use App\Domain\Event\TaskDeadlineHasBeenUpdatedEvent;
use App\Domain\Event\TaskHasBeenAssignedToUserEvent;
use App\Domain\Event\TaskHasBeenCreatedEvent;
use App\Domain\Event\TaskHasBeenDeletedEvent;
use App\Domain\Event\TaskHasBeenFinishedEvent;
use App\Domain\Event\TaskHasBeenRenamedEvent;
use App\ReadModel\Entity\TaskHistoryProjection;
use App\ReadModel\Entity\TaskProjection;
use App\ReadModel\Repository\Interfaces\TaskHistoryProjectionRepositoryInterface;

final class TaskHistoryProjector extends AbstractProjector
{
    private TaskHistoryProjectionRepositoryInterface $taskHistoryProjectionRepository;

    public function __construct(TaskHistoryProjectionRepositoryInterface $taskHistoryProjectionRepository)
    {
        $this->taskHistoryProjectionRepository = $taskHistoryProjectionRepository;
    }

    // projection apply methods

    private function createTaskHistory(AbstractTaskEvent $event): void
    {
        $taskHistory = TaskHistoryProjection::create($event->getTaskId(), (string) $event);
        $this->taskHistoryProjectionRepository->save($taskHistory);
    }

    public function applyTaskHasBeenCreatedEvent(TaskHasBeenCreatedEvent $event): void
    {
        $this->createTaskHistory($event);
    }

    public function applyTaskHasBeenRenamedEvent(TaskHasBeenRenamedEvent $event): void
    {
        $this->createTaskHistory($event);
    }

    public function applyTaskDeadlineHasBeenUpdatedEvent(TaskDeadlineHasBeenUpdatedEvent $event): void
    {
        $this->createTaskHistory($event);
    }

    public function applyTaskHasBeenAssignedToUserEvent(TaskHasBeenAssignedToUserEvent $event): void
    {
        $this->createTaskHistory($event);
    }

    public function applyTaskHasBeenFinishedEvent(TaskHasBeenFinishedEvent $event): void
    {
        $this->createTaskHistory($event);
    }

    public function applyTaskHasBeenDeletedEvent(TaskHasBeenDeletedEvent $event): void
    {
        $this->createTaskHistory($event);
    }
}
