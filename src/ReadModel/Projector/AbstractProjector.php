<?php declare(strict_types=1);

namespace App\ReadModel\Projector;

use App\Domain\Event\Interfaces\EventInterface;

abstract class AbstractProjector
{
    public function apply(EventInterface $event): void
    {
        $method = $this->getApplyMethod($event);
        $this->$method($event);
    }

    protected function getApplyMethod(EventInterface $event): string
    {
        $classNameParts = explode('\\', get_class($event));

        return 'apply' . end($classNameParts);
    }
}
