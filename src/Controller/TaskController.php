<?php declare(strict_types=1);

namespace App\Controller;

use App\Domain\AggregateRoot\Id\TaskId;
use App\Domain\Command\AssignTaskCommand;
use App\Domain\Command\CreateTaskCommand;
use App\Domain\Command\DeleteTaskCommand;
use App\Domain\Command\FinishTaskCommand;
use App\Domain\Command\RenameTaskCommand;
use App\Domain\Command\SetTaskDeadlineCommand;
use App\Domain\Entity\Id\UserId;
use App\ReadModel\Entity\TaskHistoryProjection;
use App\ReadModel\Entity\TaskProjection;
use App\ReadModel\Repository\Interfaces\TaskHistoryProjectionRepositoryInterface;
use App\ReadModel\Repository\Interfaces\TaskProjectionRepositoryInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class TaskController
{
    const OK_RESPONSE_VALUE = 'OK';

    private MessageBusInterface $commandBus;

    public function __construct(MessageBusInterface $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @Route("/tasks", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function createTask(Request $request, TaskProjectionRepositoryInterface $taskProjectionRepository)
    {
        try {
            $taskGuid = $request->request->get('taskId');
            $name = (string) $request->request->get('name');
            $deadline = $request->request->get('deadline');

            // FIXME - not nice, should be some request validator class
            if (empty($taskGuid) || empty($name)) {
                return new Response('Required params missing [taskId, name].', Response::HTTP_BAD_REQUEST);
            }

            if ($deadline) {
                $deadline = new \DateTimeImmutable($deadline);
            }

            $taskId = new TaskId(Uuid::fromString($taskGuid));
            $command = new CreateTaskCommand($taskId, $name, $deadline);
            $this->commandBus->dispatch($command);

            $task = $taskProjectionRepository->findByTaskGuid($taskGuid);

            if (!$task) {
                throw new \Exception('Could not create task.', Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return new JsonResponse($task->toArray(), Response::HTTP_CREATED);
        } catch (\Throwable $e) {
            return new Response($e->getMessage(), $e->getCode() ?: Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/tasks/{id}/name", methods={"PUT"})
     * @param string $id
     * @param Request $request
     * @return Response
     */
    public function renameTask(string $id, Request $request)
    {
        try {
            $taskId = new TaskId(Uuid::fromString($id));

            $command = new RenameTaskCommand($taskId, (string) $request->getContent());
            $this->commandBus->dispatch($command);

            return new Response(self::OK_RESPONSE_VALUE, Response::HTTP_OK);
        } catch (\Throwable $e) {
            return new Response($e->getMessage(), $e->getCode() ?: Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/tasks/{id}/assign", methods={"PUT"})
     * @param string $id
     * @param Request $request
     * @return Response
     */
    public function assignTask(string $id, Request $request)
    {
        try {
            $taskId = new TaskId(Uuid::fromString($id));
            $userId = new UserId(Uuid::fromString((string) $request->getContent()));

            $command = new AssignTaskCommand($taskId, $userId);
            $this->commandBus->dispatch($command);

            return new Response(self::OK_RESPONSE_VALUE, Response::HTTP_OK);
        } catch (\Throwable $e) {
            return new Response($e->getMessage(), $e->getCode() ?: Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/tasks/{id}/finish", methods={"PUT"})
     * @param string $id
     * @param Request $request
     * @return Response
     */
    public function finishTask(string $id, Request $request)
    {
        try {
            $taskId = new TaskId(Uuid::fromString($id));

            $command = new FinishTaskCommand($taskId);
            $this->commandBus->dispatch($command);

            return new Response(self::OK_RESPONSE_VALUE, Response::HTTP_OK);
        } catch (\Throwable $e) {
            return new Response($e->getMessage(), $e->getCode() ?: Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/tasks/{id}/deadline", methods={"PUT"})
     * @param string $id
     * @param Request $request
     * @return Response
     */
    public function deadlineTask(string $id, Request $request)
    {
        try {
            $taskId = new TaskId(Uuid::fromString($id));
            $content = (string) $request->getContent();

            $deadline = null;
            if ($content) {
                $deadline = new \DateTimeImmutable($content);
            }

            $command = new SetTaskDeadlineCommand($taskId, $deadline);
            $this->commandBus->dispatch($command);

            return new Response(self::OK_RESPONSE_VALUE, Response::HTTP_OK);
        } catch (\Throwable $e) {
            return new Response($e->getMessage(), $e->getCode() ?: Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/tasks/{id}", methods={"DELETE"})
     * @param string $id
     * @param Request $request
     * @return Response
     */
    public function deleteTask(string $id, Request $request)
    {
        try {
            $taskId = new TaskId(Uuid::fromString($id));

            $command = new DeleteTaskCommand($taskId);
            $this->commandBus->dispatch($command);

            return new Response(self::OK_RESPONSE_VALUE, Response::HTTP_OK);
        } catch (\Throwable $e) {
            return new Response($e->getMessage(), $e->getCode() ?: Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     *
     *  QUERIES
     *
     */
    /**
     * @Route("/tasks", methods={"GET"})
     */
    public function getTasks(TaskProjectionRepositoryInterface $taskProjectionRepository): Response
    {
        // FIXME move this to service class which will do the result's loading/formatting
        $tasks = $taskProjectionRepository->findAll();

        $tasks = \array_map(
            static function(TaskProjection $taskProjection) {
                return $taskProjection->toArray();
            },
            $tasks
        );

        return new JsonResponse($tasks, Response::HTTP_OK);
    }

    /**
     * @Route("/tasks/{taskGuid}", methods={"GET"})
     */
    public function getTask(string $taskGuid, TaskProjectionRepositoryInterface $taskProjectionRepository): Response
    {
        $task = $taskProjectionRepository->findByTaskGuid($taskGuid);

        if (!$task) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($task->toArray(), Response::HTTP_OK);
    }

    /**
     * @Route("/tasks/{taskGuid}/history", methods={"GET"})
     */
    public function getTaskHistory(string $taskGuid, TaskHistoryProjectionRepositoryInterface $taskHistoryProjectionRepository): Response
    {
        // FIXME move this to service class which will do the result's loading/formatting
        $taskHistory = $taskHistoryProjectionRepository->findByTaskGuid($taskGuid);

        if (!$taskHistory) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        $taskHistory = \array_map(
            static function(TaskHistoryProjection $taskHistoryProjection) {
                return $taskHistoryProjection->toArray();
            },
            $taskHistory
        );

        return new JsonResponse($taskHistory, Response::HTTP_OK);
    }
}
