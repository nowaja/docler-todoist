<?php declare(strict_types=1);

namespace App\Domain\Event;

use App\Domain\AggregateRoot\Id\TaskId;

final class TaskHasBeenFinishedEvent extends AbstractTaskEvent
{
    private \DateTimeImmutable $finishedAt;

    public function __construct(TaskId $taskId, \DateTimeImmutable $finishedAt)
    {
        $this->finishedAt = $finishedAt;

        parent::__construct($taskId);
    }

    public function getFinishedAt(): \DateTimeImmutable
    {
        return $this->finishedAt;
    }

    public function __toString(): string
    {
        return 'Task has been finished.';
    }
}
