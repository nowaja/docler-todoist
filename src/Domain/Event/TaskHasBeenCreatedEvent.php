<?php declare(strict_types=1);

namespace App\Domain\Event;

final class TaskHasBeenCreatedEvent extends AbstractTaskEvent
{
    public function __toString(): string
    {
        return 'Task has been crated.';
    }
}
