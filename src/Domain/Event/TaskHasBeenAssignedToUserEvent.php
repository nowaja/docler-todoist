<?php declare(strict_types=1);

namespace App\Domain\Event;

use App\Domain\AggregateRoot\Id\TaskId;
use App\Domain\Entity\Id\UserId;

final class TaskHasBeenAssignedToUserEvent extends AbstractTaskEvent
{
    private UserId $assignee;

    public function __construct(TaskId $taskId, UserId $assignee)
    {
        $this->assignee = $assignee;

        parent::__construct($taskId);
    }

    public function getAssignee(): UserId
    {
        return $this->assignee;
    }

    public function __toString(): string
    {
        return\sprintf('Task has been assigned to user "%s".', (string) $this->assignee);
    }
}
