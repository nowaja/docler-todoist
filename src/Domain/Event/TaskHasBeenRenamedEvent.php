<?php declare(strict_types=1);

namespace App\Domain\Event;

use App\Domain\AggregateRoot\Id\TaskId;

final class TaskHasBeenRenamedEvent extends AbstractTaskEvent
{
    private string $name;

    public function __construct(TaskId $taskId, string $name)
    {
        $this->name = $name;

        parent::__construct($taskId);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function __toString(): string
    {
        return\sprintf('Task has been renamed to "%s".', $this->name);
    }
}
