<?php declare(strict_types=1);

namespace App\Domain\Event;

use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Domain\AggregateRoot\Id\TaskId;
use App\Domain\Event\Interfaces\EventInterface;

abstract class AbstractTaskEvent implements EventInterface
{
    protected TaskId $taskId;

    public function __construct(TaskId $taskId)
    {
        $this->taskId = $taskId;
    }

    public function getAggregateId(): AggregateRootId
    {
        return $this->taskId;
    }

    public function getTaskId(): TaskId
    {
        return $this->taskId;
    }

    public function __toString(): string
    {
        return static::class;
    }
}
