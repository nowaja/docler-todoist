<?php declare(strict_types=1);

namespace App\Domain\Event;

use App\Domain\AggregateRoot\Id\TaskId;

final class TaskDeadlineHasBeenUpdatedEvent extends AbstractTaskEvent
{
    private ?\DateTimeImmutable $deadline;

    public function __construct(TaskId $taskId, ?\DateTimeImmutable $deadline)
    {
        $this->deadline = $deadline;

        parent::__construct($taskId);
    }

    public function getDeadline(): ?\DateTimeImmutable
    {
        return $this->deadline;
    }

    public function __toString(): string
    {
        return\sprintf(
            'Task deadline has been updated to "%s".',
            $this->deadline ? $this->deadline->format('Y-m-d') : null
        );
    }
}
