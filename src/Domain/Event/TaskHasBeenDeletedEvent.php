<?php declare(strict_types=1);

namespace App\Domain\Event;

use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Domain\AggregateRoot\Id\TaskId;

final class TaskHasBeenDeletedEvent extends AbstractTaskEvent
{
    private \DateTimeImmutable $deletedAt;

    public function __construct(TaskId $taskId, \DateTimeImmutable $deletedAt)
    {
        $this->deletedAt = $deletedAt;

        parent::__construct($taskId);
    }

    public function getDeletedAt(): \DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function __toString(): string
    {
        return 'Task has been deleted.';
    }
}
