<?php declare(strict_types=1);

namespace App\Domain\AggregateRoot;

use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Domain\AggregateRoot\Interfaces\AggregateRootInterface;
use App\Domain\Event\Interfaces\EventInterface;
use App\Infrastructure\EventStore\EventStream;

abstract class AbstractAggregateRoot implements AggregateRootInterface
{
    /** @var EventInterface[] */
    protected array $recordedEvents = [];

    // 0-based playhead allows events[0] to have playhead 0
    protected int $playHead = -1;

    public function getRecordedEvents(): array
    {
        return $this->recordedEvents;
    }

    public function clearRecordedEvents(): void
    {
        $this->recordedEvents = [];
    }

    public function getPlayHead(): int
    {
        return $this->playHead;
    }

    public function setPlayHead(int $playHead): void
    {
        $this->playHead = $playHead;
    }

    public function incrementPlayhead(): void
    {
        ++$this->playHead;
    }

    /**
     * @param EventInterface $event
     * @throws \Throwable
     */
    public function recordThat(EventInterface $event): void
    {
        $this->incrementPlayhead();
        $this->addRecordedEvent($event);
        $this->apply($event);
    }

    public function apply(EventInterface $event): void
    {
        $method = $this->getApplyMethod($event);
        $this->$method($event);
    }

    public static function reconstituteFromEventStream(AggregateRootId $aggregateRootId, EventStream $eventStream): AbstractAggregateRoot
    {
        $aggregate = static::createInstance($aggregateRootId);

        /** @var EventInterface $event */
        foreach ($eventStream->getEvents() as $event) {
            $aggregate->apply($event);
            $aggregate->incrementPlayHead();
        }

        return $aggregate;
    }

    abstract public static function createInstance(AggregateRootId $aggregateRootId): AbstractAggregateRoot;

    public function getApplyMethod(EventInterface $event): string
    {
        $classNameParts = explode('\\', get_class($event));

        return 'apply' . end($classNameParts);
    }

    public function addRecordedEvent(EventInterface $recordedEvent): void
    {
        $this->recordedEvents[] = $recordedEvent;
    }
}
