<?php declare(strict_types=1);

namespace App\Domain\AggregateRoot;

use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Domain\AggregateRoot\Id\TaskId;
use App\Domain\Entity\Id\UserId;
use App\Domain\Event\TaskDeadlineHasBeenUpdatedEvent;
use App\Domain\Event\TaskHasBeenAssignedToUserEvent;
use App\Domain\Event\TaskHasBeenCreatedEvent;
use App\Domain\Event\TaskHasBeenDeletedEvent;
use App\Domain\Event\TaskHasBeenFinishedEvent;
use App\Domain\Event\TaskHasBeenRenamedEvent;

final class Task extends AbstractAggregateRoot
{
    private TaskId $taskId;
    private string $name;
    private ?\DateTimeImmutable $deadline = null;
    private ?UserId $assignee = null;
    private ?\DateTimeImmutable $deletedAt = null;
    private ?\DateTimeImmutable $finishedAt = null;

    // ---------- constructors ----------

    public function __construct(TaskId $taskId)
    {
        $this->taskId = $taskId;
    }

    public static function create(TaskId $taskId, string $name, ?\DateTimeImmutable $deadline = null): self
    {
        $task = new static($taskId);

        $task->recordThat(
            new TaskHasBeenCreatedEvent($taskId),
        );

        $task->rename($name);
        $task->setDeadline($deadline);

        return $task;
    }

    public static function createInstance(AggregateRootId $aggregateRootId): AbstractAggregateRoot
    {
        if (!$aggregateRootId instanceof TaskId) {
            throw new \Exception('Cannot create Task');
        }

        return new static($aggregateRootId);
    }


// ---------- public interface ----------

    public function rename(string $name): void
    {
        if ($this->isDeleted()) {
            return;
        }

        $this->recordThat(
            new TaskHasBeenRenamedEvent($this->taskId, $name),
        );
    }

    public function setDeadline(?\DateTimeImmutable $deadline): void
    {
        if ($this->isDeleted()) {
            return;
        }

        if (!$this->isValidDeadline($deadline)) {
            return;
        }

        $this->recordThat(
            new TaskDeadlineHasBeenUpdatedEvent($this->taskId, $deadline),
        );
    }

    public function assign(UserId $assignee): void
    {
        if ($this->isDeleted()) {
            return;
        }

        if ($this->assignee instanceof UserId && (string) $this->assignee === (string) $assignee) {
            return;
        }

        $this->recordThat(
            new TaskHasBeenAssignedToUserEvent($this->taskId, $assignee),
        );
    }

    public function delete(): void
    {
        if ($this->isDeleted()) {
            return;
        }

        $this->recordThat(
            new TaskHasBeenDeletedEvent($this->taskId, new \DateTimeImmutable('now')),
        );
    }

    public function finish(): void
    {
        if ($this->isDeleted() || $this->isFinished()) {
            return;
        }

        $this->recordThat(
            new TaskHasBeenFinishedEvent($this->taskId, new \DateTimeImmutable('now')),
        );
    }

    // ---------- getters ----------
    public function getAggregateRootId(): AggregateRootId
    {
        return $this->taskId;
    }

    public function getTaskId(): TaskId
    {
        return $this->taskId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDeadline(): ?\DateTimeImmutable
    {
        return $this->deadline;
    }

    public function getAssignee(): ?UserId
    {
        return $this->assignee;
    }

    public function isDeleted(): bool
    {
        return !\is_null($this->deletedAt);
    }

    public function isFinished(): bool
    {
        return !\is_null($this->finishedAt);
    }

    // ---------- invariants handling ----------

    private function isValidDeadline(?\DateTimeImmutable $deadline): bool
    {
        if (\is_null($deadline)) {
            return !\is_null($this->deadline);
        }

        // set deadline only to future
        if (\is_null($this->deadline)) {
            return $deadline >= (new \DateTimeImmutable('now'));
        }

        return $deadline->format('Y-m-d') !== $this->deadline->format('Y-m-d') && $deadline >= (new \DateTimeImmutable('now'));
    }

    // ---------- events apply methods ----------

    public function applyTaskHasBeenCreatedEvent(TaskHasBeenCreatedEvent $event): void
    {
        $this->taskId = $event->getTaskId();
    }

    public function applyTaskHasBeenRenamedEvent(TaskHasBeenRenamedEvent $event): void
    {
        $this->name = $event->getName();
    }

    public function applyTaskDeadlineHasBeenUpdatedEvent(TaskDeadlineHasBeenUpdatedEvent $event): void
    {
        $this->deadline = $event->getDeadline();
    }

    public function applyTaskHasBeenAssignedToUserEvent(TaskHasBeenAssignedToUserEvent $event): void
    {
        $this->assignee = $event->getAssignee();
    }

    public function applyTaskHasBeenDeletedEvent(TaskHasBeenDeletedEvent $event): void
    {
        $this->deletedAt = $event->getDeletedAt();
    }

    public function applyTaskHasBeenFinishedEvent(TaskHasBeenFinishedEvent $event): void
    {
        $this->finishedAt = $event->getFinishedAt();
    }
}
