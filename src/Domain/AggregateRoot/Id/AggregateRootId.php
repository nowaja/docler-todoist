<?php declare(strict_types=1);

namespace App\Domain\AggregateRoot\Id;

use Ramsey\Uuid\UuidInterface;

class AggregateRootId
{
    protected string $uuid;

    public function __construct(UuidInterface $uuid)
    {
        $this->uuid = (string) $uuid;
    }

    public function getId(): string
    {
        return $this->uuid;
    }

    public function __toString(): string
    {
        return $this->uuid;
    }
}