<?php declare(strict_types=1);

namespace App\Domain\AggregateRoot\Interfaces;

use App\Domain\AggregateRoot\AbstractAggregateRoot;
use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Domain\Event\AbstractTaskEvent;
use App\Domain\Event\Interfaces\EventInterface;
use App\Infrastructure\EventStore\EventStream;

interface AggregateRootInterface
{
    public static function reconstituteFromEventStream(AggregateRootId $aggregateRootId, EventStream $eventStream): AbstractAggregateRoot;

    public function getRecordedEvents(): array;

    public function clearRecordedEvents(): void;

    public function recordThat(EventInterface $event): void;

    public function apply(EventInterface $event): void;

    public function getApplyMethod(EventInterface $event): string;

    public function addRecordedEvent(EventInterface $recordedEvent): void;

    public function getPlayHead(): int;

    public function setPlayHead(int $playHead): void;

    public function incrementPlayHead(): void;

    public function getAggregateRootId(): AggregateRootId;
}
