<?php declare(strict_types=1);

namespace App\Domain\EventHandler\Interfaces;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

interface SyncEventHandlerInterface extends MessageHandlerInterface
{

}