<?php declare(strict_types=1);

namespace App\Domain\EventHandler;

use App\Domain\Event\AbstractTaskEvent;
use App\Domain\EventHandler\Interfaces\SyncEventHandlerInterface;
use App\ReadModel\Projector\TaskHistoryProjector;
use App\ReadModel\Projector\TaskProjector;

class TaskEventHandler implements SyncEventHandlerInterface
{
    protected TaskProjector $taskProjector;
    protected TaskHistoryProjector $taskHistoryProjector;

    public function __construct(TaskProjector $taskProjector, TaskHistoryProjector $taskHistoryProjector)
    {
        $this->taskProjector = $taskProjector;
        $this->taskHistoryProjector = $taskHistoryProjector;
    }

    public function __invoke(AbstractTaskEvent $event): void
    {
        $this->taskProjector->apply($event);
        $this->taskHistoryProjector->apply($event);
    }
}
