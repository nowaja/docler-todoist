<?php declare(strict_types=1);

namespace App\Domain\Command;

use App\Domain\AggregateRoot\Id\TaskId;

final class SetTaskDeadlineCommand extends AbstractTaskCommand
{
    private ?\DateTimeImmutable $deadline;

    public function __construct(TaskId $taskId, ?\DateTimeImmutable $deadline = null)
    {
        $this->deadline = $deadline;

        parent::__construct($taskId);
    }

    public function getDeadline(): ?\DateTimeImmutable
    {
        return $this->deadline;
    }
}
