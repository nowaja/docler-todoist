<?php declare(strict_types=1);

namespace App\Domain\Command;

use App\Domain\AggregateRoot\Id\TaskId;

class AbstractTaskCommand
{
    protected TaskId $taskId;

    public function __construct(TaskId $taskId)
    {
        $this->taskId = $taskId;
    }

    public function getTaskId(): TaskId
    {
        return $this->taskId;
    }
}
