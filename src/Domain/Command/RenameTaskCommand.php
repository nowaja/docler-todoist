<?php declare(strict_types=1);

namespace App\Domain\Command;

use App\Domain\AggregateRoot\Id\TaskId;

final class RenameTaskCommand extends AbstractTaskCommand
{
    private string $name;

    public function __construct(TaskId $taskId, string $name)
    {
        $this->name = $name;

        parent::__construct($taskId);
    }

    public function getName(): string
    {
        return $this->name;
    }
}
