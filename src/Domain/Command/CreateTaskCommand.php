<?php declare(strict_types=1);

namespace App\Domain\Command;

use App\Domain\AggregateRoot\Id\TaskId;

final class CreateTaskCommand extends AbstractTaskCommand
{
    private string $name;
    private ?\DateTimeImmutable $deadline;

    public function __construct(TaskId $taskId, string $name, ?\DateTimeImmutable $deadline)
    {
        $this->name = $name;
        $this->deadline = $deadline;

        parent::__construct($taskId);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDeadline(): ?\DateTimeImmutable
    {
        return $this->deadline;
    }
}
