<?php declare(strict_types=1);

namespace App\Domain\Command;

use App\Domain\AggregateRoot\Id\TaskId;
use App\Domain\Entity\Id\UserId;

final class AssignTaskCommand extends AbstractTaskCommand
{
    private UserId $assignee;

    public function __construct(TaskId $taskId, UserId $assignee)
    {
        $this->assignee = $assignee;

        parent::__construct($taskId);
    }

    public function getAssignee(): UserId
    {
        return $this->assignee;
    }
}
