<?php declare(strict_types=1);

namespace App\Domain\CommandHandler;

use App\Domain\Command\AssignTaskCommand;

final class AssignTaskCommandHandler extends AbstractTaskCommandHandler
{
    public function __invoke(AssignTaskCommand $command): void
    {
        $task = $this->taskRepository->findTask($command->getTaskId());

        if (!$task) {
            throw new \Exception('Task does not exist.', 404); // FIXME - custom exceptions
        }

        $task->assign($command->getAssignee());

        $this->taskRepository->save($task);
    }
}
