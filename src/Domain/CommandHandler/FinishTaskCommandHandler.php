<?php declare(strict_types=1);

namespace App\Domain\CommandHandler;

use App\Domain\Command\FinishTaskCommand;

final class FinishTaskCommandHandler extends AbstractTaskCommandHandler
{
    public function __invoke(FinishTaskCommand $command): void
    {
        $task = $this->taskRepository->findTask($command->getTaskId());

        if (!$task) {
            throw new \Exception('Task does not exist.', 404); // FIXME - custom exceptions
        }

        $task->finish();

        $this->taskRepository->save($task);
    }
}
