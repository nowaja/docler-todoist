<?php declare(strict_types=1);

namespace App\Domain\CommandHandler;

use App\Domain\Command\RenameTaskCommand;

final class RenameTaskCommandHandler extends AbstractTaskCommandHandler
{
    public function __invoke(RenameTaskCommand $command): void
    {
        $task = $this->taskRepository->findTask($command->getTaskId());

        if (!$task) {
            throw new \Exception('Task does not exist.', 404); // FIXME - custom exceptions
        }

        $task->rename($command->getName());

        $this->taskRepository->save($task);
    }
}
