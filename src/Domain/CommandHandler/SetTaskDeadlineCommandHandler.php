<?php declare(strict_types=1);

namespace App\Domain\CommandHandler;

use App\Domain\Command\SetTaskDeadlineCommand;

final class SetTaskDeadlineCommandHandler extends AbstractTaskCommandHandler
{
    public function __invoke(SetTaskDeadlineCommand $command): void
    {
        $task = $this->taskRepository->findTask($command->getTaskId());

        if (!$task) {
            throw new \Exception('Task does not exist.', 404); // FIXME - custom exceptions
        }

        $task->setDeadline($command->getDeadline());

        $this->taskRepository->save($task);
    }
}
