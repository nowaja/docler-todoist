<?php declare(strict_types=1);

namespace App\Domain\CommandHandler;

use App\Domain\Command\DeleteTaskCommand;

final class DeleteTaskCommandHandler extends AbstractTaskCommandHandler
{
    public function __invoke(DeleteTaskCommand $command): void
    {
        $task = $this->taskRepository->findTask($command->getTaskId());

        if (!$task) {
            throw new \Exception('Task does not exist.', 404); // FIXME - custom exceptions
        }

        $task->delete();

        $this->taskRepository->save($task);
    }
}
