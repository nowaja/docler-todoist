<?php declare(strict_types=1);

namespace App\Domain\CommandHandler\Interfaces;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

interface SyncCommandHandlerInterface extends MessageHandlerInterface
{

}