<?php declare(strict_types=1);

namespace App\Domain\CommandHandler;

use App\Domain\AggregateRoot\Task;
use App\Domain\Command\CreateTaskCommand;

final class CreateTaskCommandHandler extends AbstractTaskCommandHandler
{
    public function __invoke(CreateTaskCommand $command): void
    {
        $task = $this->taskRepository->findTask($command->getTaskId());

        if ($task) {
            throw new \Exception('Task already exists.', 409); // FIXME - custom exceptions
        }

        $task = Task::create($command->getTaskId(), $command->getName(), $command->getDeadline());

        $this->taskRepository->save($task);
    }
}
