<?php declare(strict_types=1);

namespace App\Domain\CommandHandler;

use App\Domain\CommandHandler\Interfaces\SyncCommandHandlerInterface;
use App\Domain\Repository\TaskRepository;

abstract class AbstractTaskCommandHandler implements SyncCommandHandlerInterface
{
    protected TaskRepository $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }
}
