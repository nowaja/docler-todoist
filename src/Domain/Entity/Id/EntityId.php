<?php declare(strict_types=1);

namespace App\Domain\Entity\Id;

use Ramsey\Uuid\UuidInterface;

class EntityId
{
    protected string $uuid;

    public function __construct(UuidInterface $uuid)
    {
        $this->uuid = (string) $uuid;
    }

    public function getId(): string
    {
        return $this->uuid;
    }

    public function __toString(): string
    {
        return $this->uuid;
    }
}