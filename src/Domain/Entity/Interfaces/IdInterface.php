<?php declare(strict_types=1);

namespace App\Domain\Entity\Interfaces;

interface IdInterface
{
    public function getId(): ?int;
}
