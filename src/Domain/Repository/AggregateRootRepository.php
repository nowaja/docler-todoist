<?php declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Domain\AggregateRoot\AbstractAggregateRoot;
use App\Infrastructure\EventStore\Interfaces\EventStoreInterface;
use App\Infrastructure\EventStore\EventStream;
use App\Infrastructure\EventStore\Exceptions\ConcurrencyException;

class AggregateRootRepository
{
    private EventStoreInterface $eventStore;

    public function __construct(EventStoreInterface $eventStore)
    {
        $this->eventStore = $eventStore;
    }

    protected function loadEventStream(string $uuid): ?EventStream
    {
        return $this->eventStore->load($uuid);
    }

    public function save(AbstractAggregateRoot $aggregateRoot): void
    {
        $eventStream = $this->loadEventStream($aggregateRoot->getAggregateRootId()->getId());

        $lastSavedPlayhead = -1;

        if ($eventStream) {
            $events = $eventStream->getEvents();
            $lastSavedPlayhead = count($events) - 1;

            if ($lastSavedPlayhead + count($aggregateRoot->getRecordedEvents()) !== $aggregateRoot->getPlayHead()) {
                throw new ConcurrencyException();
            }
        }

        $this->eventStore->append($aggregateRoot->getRecordedEvents(), $lastSavedPlayhead);

        $aggregateRoot->clearRecordedEvents();
    }

    protected function get(AggregateRootId $aggregateRootId, string $className): ?AbstractAggregateRoot
    {
        $eventStream = $this->loadEventStream($aggregateRootId->getId());

        if(!$eventStream) {
            return null;
        }

        /** @var AbstractAggregateRoot $className */
        return $className::reconstituteFromEventStream($aggregateRootId, $eventStream);
    }
}
