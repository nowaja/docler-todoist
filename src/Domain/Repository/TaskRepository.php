<?php declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Domain\AggregateRoot\Task;

class TaskRepository extends AggregateRootRepository
{
    public function findTask(AggregateRootId $aggregateRootId): ?Task
    {
        $task = parent::get($aggregateRootId, Task::class);

        if ($task instanceof Task) {
            return $task;
        }

        return null;
    }
}
